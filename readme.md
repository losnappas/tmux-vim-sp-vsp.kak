# tmux-vim-sp-vsp.kak

`:sp`, `:sp file` and `:vsp` from Vim. The difference between these and `:new` is that these open a new shell so you can use the ["suspend-and-resume"](https://github.com/mawww/kakoune/wiki/Integrating-Other-CLI-apps) paradigm with tmux and ctrl-z in general.

## Installation with plug.kak.

```
plug 'https://gitlab.com/losnappas/tmux-vim-sp-vsp.kak' config %{
	alias global sp tmux-vim-vertical
	alias global vsp tmux-vim-horizontal
	# # OR, if you prefer it to "swallow" the terminal
	# # check out https://gitlab.com/losnappas/tmux-swallow-terminal.kak
	# alias global sp tmux-vim-swallow-vertical
	# alias global vsp tmux-vim-swallow-horizontal
}
```

Yep. Vertical is horizontal in Kakoune. :rolleyes:

Otherwise clone it into you autoload dir. (Probs `~/.config/kak/autoload`)
