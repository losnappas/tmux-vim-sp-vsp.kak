define-command -params 0..1 tmux-vim-vertical -file-completion %{
    tmux-repl-vertical
    tmux-vim-impl %arg{@}
}
define-command -params 0..1 tmux-vim-horizontal -file-completion %{
    tmux-repl-horizontal
    tmux-vim-impl %arg{@}
}
define-command -hidden -params 0..1 tmux-vim-impl %{
    nop %sh{
        args="${kak_buffile}"
        if [ $# -eq 1 ]; then
            args="$@"
        fi
        tmux send-keys "kak -c $kak_session $args" Enter
    }
}
define-command -params 0..1 tmux-vim-swallow-vertical -file-completion %{
    tmux-repl-vertical
    tmux-vim-swallow-impl %arg{@}
}
define-command -params 0..1 tmux-vim-swallow-horizontal -file-completion %{
    tmux-repl-horizontal
    tmux-vim-swallow-impl %arg{@}
}
define-command -hidden -params 0..1 tmux-vim-swallow-impl %{
    nop %sh{
        args="${kak_buffile}"
        if [ $# -eq 1 ]; then
            args="$@"
        fi
        tmux send-keys "tmux split-window -bh 'kak -c $kak_session $args' & ; disown && sleep 0.01 && exit" Enter
    }
}
